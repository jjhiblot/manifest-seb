# SEB Multimedia Platform Repo Manifest README

This repo is used to download manifests for Multimedia Platform releases.

To use this manifest repo, the 'repo' tool must be isntalled first.
--------------------------------------------------------
    $ mkdir ~/bin
    $ curl http://commondatastorage.googleapis.com/git-repo-downloads/repo  > ~/bin/repo
    $ chmod a+x ~/bin/repo
    $ PATH=${PATH}:~/bin

To execute:

    $ mkdir <release>
    $ cd <release>
    $ repo init -u <TODO fill url > -b <branch name> [ -m <release manifest>]
    $ repo sync

Examples
--------
To download the development manifest:

    $ repo init -u <TODO fill url>  -b master -m dev.xml

Setup the build folder for a BSP release:
-----------------------------------------

    $ . seb-setup-release.sh [<build directory>]

Build an image:
---------------

    $ bitbake <image recipe>

Some image recipes:

* seb-image-qt5-dev : image with QT5, used for application development (unsafe)
* seb-image-qt5 : production image (no ssh, etc.)

Dev Tips:
---------

Downloads can be shared across multiple builds.
Modify your conf/local.conf with:

    DL_DIR = "/path/to/downloads"

To make rebuild faster build yocto populates a sstate-cache. After generation a sstate-cache can be share with other builds.
This can (very)significantly reduces the time/space required for the other builds.
Modify your conf/local.conf to add:

    SSTATE_MIRRORS = "file://.* file:///path/to/another/build/sstate-cache/PATH"

It is also possible to share the sstate over http of NFS.
